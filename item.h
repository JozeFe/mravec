#pragma once
#include "SDL/SDL.h"
#include <iostream>
#include "IO.h"

class Citem
{
protected:
	int m_x;
	int m_y;
	SDL_Rect m_box;
public:
	Citem(int x, int y) : m_x(x), m_y(y) 
	{
		m_box.x=x + 390;
		m_box.y=y + 300;
		m_box.w=RECTSIZE;
		m_box.h=RECTSIZE;
	}
	
	void setX(int x)
	{
		m_box.x = x;
	}
	
	void setY(int y)
	{
		m_box.y = y;
	}
	
	virtual void draw(SDL_Surface *screen, int x, int y) = 0;
	virtual ~Citem(){};
	
};

class Cwall : public Citem
{	
public:
	Cwall(int x, int y) : Citem(x, y) {} 
	
	virtual void draw(SDL_Surface *screen, int x, int y)
	{
		m_box.x = m_x + 390 - x;
		m_box.y = m_y + 300 - y;
		if (((m_x + 390 - x) >= 0) && ((m_x + 390 - x) <= 750)
		&& ((m_y + 300 - y) >= 0) && ((m_y + 300 - y) <= 570))
			SDL_FillRect(screen,&m_box, 0xFF9900); // oranzova
		
	}
	virtual ~Cwall(){};
	
};

class Cpath : public Citem
{	
private:
	bool m_help;
public:
	Cpath(int x, int y) : Citem(x, y){ m_help = false; } 
	
	virtual void draw(SDL_Surface *screen, int x, int y)
	{
		if (isHelp() == true ) {
			m_box.x = m_x + 390 - x;
			m_box.y = m_y + 300 - y;
			if (((m_x + 390 - x) >= 0) && ((m_x + 390 - x) <= 750)
			&& ((m_y + 300 - y) >= 0) && ((m_y + 300 - y) <= 570))
				SDL_FillRect(screen,&m_box, 0xBFBFBF); // siva
		}
	}
	virtual ~Cpath(){};
	
	bool isHelp(){ return m_help; }
	void setHelp() { m_help ^= 1; }
	
};

class Cstart : public Citem
{	
public:
	Cstart(int x, int y) : Citem(x, y) {} 
	
	virtual void draw(SDL_Surface *screen, int x, int y)
	{
		m_box.x = m_x + 390 - x;
		m_box.y = m_y + 300 - y;
		if (((m_x + 390 - x) >= 0) && ((m_x + 390 - x) <= 750)
		&& ((m_y + 300 - y) >= 0) && ((m_y + 300 - y) <= 570))
			SDL_FillRect(screen,&m_box, 0x000000); // cervena
	}
	virtual ~Cstart(){};
	
};

class Cfinish : public Citem
{	
public:
	Cfinish(int x, int y) : Citem(x, y) {} 
	
	virtual void draw(SDL_Surface *screen, int x, int y)
	{
		m_box.x = m_x + 390 - x;
		m_box.y = m_y + 300 - y;
		if (((m_x + 390 - x) >= 0) && ((m_x + 390 - x) <= 750)
		&& ((m_y + 300 - y) >= 0) && ((m_y + 300 - y) <= 570))
			SDL_FillRect(screen,&m_box, 0x18D80E); // zelena
	}
	
	virtual ~Cfinish(){};
};
