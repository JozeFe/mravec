#pragma once 
#include <iostream>
#include "FileReader.h"
#include "IO.h"
#include "SDL/SDL.h"
#include "item.h"
#include "character.h"
#include "MazeSolver.h"

using namespace std;

class Cgui
{
private:
	vector<Citem*> m_items;
	vector<vector<int> > m_maze;
	SDL_Surface *m_screen;
	Ccharacter *m_hero;
	bool m_count;
	
	//------------------------------------------------------------------------
	// vytvori graficke okno programu 780x600
	void CreateWindow()
	{
		m_count = false;
		SDL_Init( SDL_INIT_EVERYTHING );
		m_screen = SDL_SetVideoMode( 780, 600, 32, SDL_HWSURFACE);
		if ( m_screen == NULL ) {
			fprintf(stderr, "Problem: %s\n", SDL_GetError());
			exit(1);
		}
	}
	
	//------------------------------------------------------------------------
	// vytvori a nacita objekty mapy do vektora m_items
	void ItemsAlocation()
	{
		for (int y = 0; y < SIZE; y++) 
		{
			for (int x = 0; x < SIZE; x++)
			{
				Citem *tmp;
				switch (m_maze[x][y])
				{
					case WALL:
						tmp = new Cwall(x * RECTSIZE,y * RECTSIZE);
						break;
					case START:
						tmp = new Cstart(x * RECTSIZE,y * RECTSIZE);
						m_hero = new Ccharacter(x * RECTSIZE,y * RECTSIZE);
						break;
					case FINISH:
						tmp = new Cfinish(x * RECTSIZE,y * RECTSIZE);
						break;
					case PATH:
						tmp = new Cpath(x * RECTSIZE,y * RECTSIZE);
						break;
					default:
						break;
				}
				m_items.push_back(tmp);
			}
		}
	}
	
public:
	Cgui()
	{
		CFileReader *fr = new CFileReader("solution.txt"); 
		m_maze = fr->getMaze();
		
		CreateWindow();
		ItemsAlocation();
		
		SDL_Event event;
		
		while(true)
		{
			while(SDL_PollEvent(&event))
			{
				switch (event.type) 
				{
					case SDL_QUIT:
						exit(0);
					case SDL_KEYDOWN:
						if(event.key.keysym.sym==SDLK_LEFT){
							if(m_maze[(m_hero->getX())/RECTSIZE -1][(m_hero->getY())/RECTSIZE] != WALL)
								m_hero->setX(-RECTSIZE);
						}
						if(event.key.keysym.sym==SDLK_RIGHT){
							if(m_maze[(m_hero->getX())/RECTSIZE +1][(m_hero->getY())/RECTSIZE] != WALL)
								m_hero->setX(RECTSIZE);
						}
						if(event.key.keysym.sym==SDLK_UP){
							if(m_maze[(m_hero->getX())/RECTSIZE][(m_hero->getY())/RECTSIZE - 1] != WALL)
								m_hero->setY(-RECTSIZE);
						}
						if(event.key.keysym.sym==SDLK_DOWN){
							if(m_maze[(m_hero->getX())/RECTSIZE][(m_hero->getY())/RECTSIZE + 1] != WALL)
								m_hero->setY(RECTSIZE);
						}
						if(event.key.keysym.sym==SDLK_h){
							m_count ^= 1;
							if (m_count)
							{
								m_items.clear();
								CMazeSolver mz(m_maze, m_hero->getX()/RECTSIZE, m_hero->getY()/RECTSIZE);
								m_maze = mz.breadthFirstSearch();
								ItemsAlocation();
								for (unsigned int i = 0; i < m_items.size(); i++)
								{
									Cpath* v = dynamic_cast<Cpath*>(m_items[i]);
									if(v != 0) {
										// old was safely casted to NewType
										v->setHelp();
									}
								}
							} else {
								for (unsigned int i = 0; i < m_items.size(); i++)
								{
									Cpath* v = dynamic_cast<Cpath*>(m_items[i]);
									if(v != 0) {
										// old was safely casted to NewType
										v->setHelp();
									}
								}
							}
						break;
					}
				}
				
				SDL_FillRect(m_screen,NULL, 0x000000);
				for (unsigned int i = 0; i < m_items.size(); i++)
				{
					m_items[i]->draw(m_screen, m_hero->getX(), m_hero->getY());
				}
				m_hero->draw(m_screen);
				SDL_UpdateRect(m_screen,0,0,0,0);		
			}
			if (m_maze[(m_hero->getX())/30][(m_hero->getY())/30] == FINISH)
				break;
		}	
	}
};
