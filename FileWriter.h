#pragma once

#include "IO.h"
#include <fstream>
#include <string>
#include <iostream>
#include <vector>

using namespace std;

class CFileWriter
{
private:
	string m_fileName;
	
public:

	//------------------------------------------------------------------------
	// konstruktor, zapise vygenerovane bludisko do suboru...
	CFileWriter(vector<vector<int> > maze, string f = "default.txt")
	{
		m_fileName = f;
		ofstream fout;
		fout.open(m_fileName.c_str());

		if (!fout.is_open()){
			cerr << "Subor " << m_fileName << " sa nepodarilo otvorit" << endl;
			return;
		}
		for (int y = 0; y < SIZE; y++)
		{
			for (int x = 0; x < SIZE; x++)
			{
				fout << maze[x][y];
			}
			fout << endl;
		}
		fout.close();
	}
	
	//------------------------------------------------------------------------
	//destruktor
	~CFileWriter()
	{
		
	}
};

