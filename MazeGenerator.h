#pragma once
#define _USE_MATH_DEFINES
#include "IO.h"
#include <cstdlib>
#include <ctime>  
#include <cmath>

#include "FileWriter.h"

using namespace std;

struct TPair
{
	int m_X;
	int m_Y;
};

class CMazeGenerator
{
private:
	vector<vector<int> > m_maze;
	
	//------------------------------------------------------------------------
	// vygeneruje nahodne zaciatok a koniec v bludisku
	int randomizeStartFinish()
	{
		int rnd = rand() % SIZE - 2 + 1; // random od 1 do 27
		rnd = abs(rnd);
		if (rnd % 2 == 0)
			rnd++;

		return rnd;
	}
	
	//-------------------------------------------------------------------------
	// vygeneruje nahodne steny bludiska
	void randomizeWalls()
	{
		int pillars; // zakladne kamene
		do
		{
			vector<TPair> myPillars; // arrayList zakladnych kamenov
			pillars = 0;
			for (int y = 0; y < SIZE; y++) // prechadza postupne cele bludisko a hlada zakladne kamene....
			{
				for (int x = 0; x < SIZE; x++)
				{
					if (m_maze[x][y] == BASE) // ak nejaky najde
					{
						pillars++;
						TPair p;
						p.m_X = x;
						p.m_Y = y;
						myPillars.push_back(p); // tak ho ulozi do vectora ako strukturu TPair (TPair ma 2 int hodnoty xksovu suradnicu a ypsilonovu..)
					}
				}
			}

			if (pillars > 0) // ak sa nachdzaju nejake zakladne kamene v bludisku
			{
				int rnd = rand() % myPillars.size(); // tak si nahodne nejaky vyber z toho arrayListu
				TPair tmp;
				tmp = myPillars[rnd];

				int direction = rand() % 4; // vyber si nahodne nejaky smer....
				// a z toho miesta co som si vybral z toho arraylistu davam 1dnotky v smere aky sa nahodne vybral... az kym nenarazim na inu stenu
				if (direction == UP)
				{
					m_maze[tmp.m_X][tmp.m_Y] = WALL;
					while (m_maze[tmp.m_X - 1][tmp.m_Y] != WALL)
					{
						tmp.m_X--;
						m_maze[tmp.m_X][tmp.m_Y] = WALL;
					}
				}
				if (direction == DOWN)
				{
					m_maze[tmp.m_X][tmp.m_Y] = WALL;
					while (m_maze[tmp.m_X + 1][tmp.m_Y] != WALL)
					{
						tmp.m_X++;
						m_maze[tmp.m_X][tmp.m_Y] = WALL;
					}
				}
				if (direction == LEFT)
				{
					m_maze[tmp.m_X][tmp.m_Y] = WALL;
					while (m_maze[tmp.m_X][tmp.m_Y - 1] != WALL)
					{
						tmp.m_Y--;
						m_maze[tmp.m_X][tmp.m_Y] = WALL;
					}
				}
				if (direction == RIGHT)
				{
					m_maze[tmp.m_X][tmp.m_Y] = WALL;
					while (m_maze[tmp.m_X][tmp.m_Y + 1] != WALL)
					{
						tmp.m_Y++;
						m_maze[tmp.m_X][tmp.m_Y] = WALL;
					}
				}
			}
			myPillars.clear();
		} while (pillars); // tento algoritmus robim kym vsetko nezastaviam ;)
	}
public:
	
	//------------------------------------------------------------------------
	// kontruktor nastavi dvojrozmerny vector do zakladneho stavu...
	CMazeGenerator()
	{
		m_maze.resize(SIZE); // vytvori pole a velkosti size napr pole[28]
		for (int i = 0; i < SIZE; i++) // vytvori ku kazdemu polu dalsie pole cize ako keby pole[28][28]
		{
			m_maze[i].resize(SIZE);
		}

		// pripravuje zaklad bludiska, steny a zakladne kamene
		for (int y = 0; y < SIZE; y++)
		{
			for (int x = 0; x < SIZE; x++)
			{
				if (y == 0 || y == SIZE - 1) // nastavi okraje bludiska na stenu....
					m_maze[x][y] = WALL;
				else if (x == 0 || x == SIZE - 1) // nastavi okraje bludiska na stenu....
					m_maze[x][y] = WALL;
				else if (x % 2 == 0 && y % 2 == 0) // nastavi zakladne "kamene" na vystavbu steny...
					m_maze[x][y] = BASE;
				else
					m_maze[x][y] = EMPTY; // vsetko ostatne vynuluje
			}
		}

		srand(time(NULL));
		int rnd = randomizeStartFinish(); // nastavi v 1. stlpci random zaciatok
		m_maze[1][rnd] = START;

		rnd = randomizeStartFinish(); // nastavi v predposlednom stlpci random koniec...
		m_maze[SIZE - 2][rnd] = FINISH;

		randomizeWalls(); // vygeneruje steny

		CFileWriter fw(m_maze);
	}
	
	//------------------------------------------------------------------------
	// destruktor
	~CMazeGenerator() { };
};

