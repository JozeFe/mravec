#pragma once

#include "IO.h"
#include "MazeGenerator.h"
#include <deque>
#include <cstdlib> 
#include <stdlib.h>

class CMazeSolver
{
private:
	vector<vector<int> > m_maze;
	int myX;
	int myY;

	//------------------------------------------------------------------------
	//vnorena trieda CPair, sluzi ako novy "datovy typ" - na zapametanie x-sovej, y-lonovej suradnice policka...
	class CPair
	{
	public:
		int m_X;
		int m_Y;
	};

	deque<CPair*> m_q; // zasobnik
	
	//------------------------------------------------------------------------
	// pomocna metoda na najdenie zaciatku bludiska
	void findStart()
	{
		if (myX != 0 || myY != 0)
		{
			findHero();
			return;
		}
		int x(1); int y(1);

		while (true)
		{
			if (m_maze[x][y] == START)
				break;
			y++;
		}
		CPair *t = new CPair();
		t->m_X = x;
		t->m_Y = y;
		m_q.push_back(t);
		m_maze[x][y] = 6;
	}
	
	void findHero()
	{
		CPair *t = new CPair();
		t->m_X = myX;
		t->m_Y = myY;
		m_q.push_back(t);
		m_maze[myX][myY] = 6;
			
		for (int y = 0; y < SIZE; y++) // vypis konecneho vyrieseneho bludiska...
		{
			for (int x = 0; x < SIZE; x++)
			{
				if (m_maze[x][y] == PATH || m_maze[x][y] == START)
					m_maze[x][y] = 0;
			}
		}
	}
	
	//------------------------------------------------------------------------
	// pomocna metoda na debugovanie, vypisanie bludiska na terminal
	void showMaze()
	{
		for (int y = 0; y < SIZE; y++) // vypis konecneho vyrieseneho bludiska...
		{
			for (int x = 0; x < SIZE; x++)
			{
				cout << m_maze[x][y];
			}
			cout << endl;
		}
	}
	
	//------------------------------------------------------------------------
	//nevyuzite cesty vrati spat do povodneho stavu (slepe ulicky)
	void backToNormal()
	{
		for (int y = 0; y < SIZE; y++) // vynuluje nepouzite policka....
		{
			for (int x = 0; x < SIZE; x++)
			{
				if (m_maze[x][y] > 6)
					m_maze[x][y] = 0;
				if (m_maze[x][y] == 6)
					m_maze[x][y] = START;
			}
		}
	}
public:

	//------------------------------------------------------------------------
	// konstruktor
	CMazeSolver(vector<vector<int> > maze, int x = 0, int y = 0)
	{
		m_maze = maze;
		myX = x;
		myY = y;
	}
	
	//------------------------------------------------------------------------
	//algoritmus "breadthFirstSearch" sirenie do sirky, najde najkratsiu
	//cestu v bludisku zo startu k cielu...
	vector<vector<int> > breadthFirstSearch()
	{
		findStart(); // najde zaciatok
			
		CPair *temp = new CPair();
		int myWay = 6; // oznaci zaciatok na 6tku (prve nevyuzivane cislo)
		double m(0);
		bool condition = true;
		while (condition)
		{
			temp = m_q.front(); // vyberie z fronty policko zaciatok
			m_q.pop_front();
		
			// prechadza z daneho policka vo vsetkych 4och smeroch, ak na danom policku este nebol
			// nastavi mu o 1 vacsie cislo ako na ktorom teraz stoji, ak najde policko koniec ukonci
			// cyklus. Nove policka dava na koniec zasobnika, vybera zo zaciatku zasobnika (tie najstrasie)
			for (int i = 0; i < 4; i++)
			{
				m += 0.5;
				if (m_maze[temp->m_X + (int)nearbyint(sin(m* M_PI))][temp->m_Y + (int)nearbyint(cos(m* M_PI))] == 0 
					|| m_maze[temp->m_X + (int)nearbyint(sin(m* M_PI))][temp->m_Y + (int)nearbyint(cos(m* M_PI))] == FINISH)
				{
					myWay = m_maze[temp->m_X][temp->m_Y]; // zisti hodnotu policka na ktorom stoji...

					if (m_maze[temp->m_X + (int)nearbyint(sin(m* M_PI))][temp->m_Y + (int)nearbyint(cos(m* M_PI))] == FINISH) // ak sa pozeram na policko koniec....
					{
						m_q.clear(); // vycisti cely zasobnik
						CPair *f = new CPair(); // vytvorim si objekt a inicializujem ho na suradnice cielu...
						f->m_X = temp->m_X + (int)nearbyint(sin(m* M_PI));
						f->m_Y = temp->m_Y + (int)nearbyint(cos(m* M_PI));
						m_q.push_back(f); // do zasobnika vlozim suradnice cielu
						condition = false;
					}

					m_maze[temp->m_X + (int)nearbyint(sin(m* M_PI))][temp->m_Y + (int)nearbyint(cos(m* M_PI))] = myWay + 1; // ak sa pozeram na prazdne policko tak zvysim jeho hodnotu o 1 (od toho na ktorom stojim)
					CPair *a = new CPair(); // vytvorim si objekt a inicializujem ho na suradnice policka na ktore sa pozeram...
					a->m_X = temp->m_X + (int)nearbyint(sin(m* M_PI));
					a->m_Y = temp->m_Y + (int)nearbyint(cos(m* M_PI));
					m_q.push_back(a); // suradnice policka vlozim do zasobnika
				}
			}
			m = 0; 
		}
		CPair *backw = new CPair();
		backw = m_q.front();
		m_maze[backw->m_X][backw->m_Y] = FINISH;
		zaciatok:
		while (myWay != 6) // pokial sa nedostanem k startu
		{
			m = 0;
			for (int i = 0; i < 4; i++) // prechadzam od konca spatne vsetky policka vo vsetkych smeroch....
			{
				m += 0.5;
				if (m_maze[backw->m_X + (int)nearbyint(sin(m* M_PI))][backw->m_Y + (int)nearbyint(cos(m* M_PI))] == myWay)
				{
					m_maze[backw->m_X + (int)nearbyint(sin(m* M_PI))][backw->m_Y + (int)nearbyint(cos(m* M_PI))] = PATH;
					backw->m_X = backw->m_X + (int)nearbyint(sin(m* M_PI));
					backw->m_Y = backw->m_Y + (int)nearbyint(cos(m* M_PI));
					myWay--;
					goto zaciatok;
				}
			}
		}
		backToNormal();
		
		for (int y = 0; y < SIZE; y++) // vypis konecneho vyrieseneho bludiska...
		{
			for (int x = 0; x < SIZE; x++)
			{
				if (m_maze[x][y] == 6 )
					m_maze[x][y] = START;
			}
		}
		return m_maze;
	}
	
	//------------------------------------------------------------------------
	
	~CMazeSolver() {};
};

