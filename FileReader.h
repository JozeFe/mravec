#pragma once

#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include "FileWriter.h"

using namespace std;

class CFileReader
{
private:
	vector<vector<int> > m_readMaze;
	string m_fileName;
	
public:

	//------------------------------------------------------------------------

	CFileReader(string f = "default.txt")
	{
		m_readMaze.resize(SIZE); // vytvori pole a velkosti size napr pole[29]
		for (int i = 0; i < SIZE; i++) // vytvori ku kazdemu polu dalsie pole cize ako keby pole[29][29]
		{
			m_readMaze[i].resize(SIZE);
		}

		m_fileName = f;
		ifstream fin;
		fin.open(m_fileName.c_str()); // otvori subor

		int y, x;
		y = x = 0;
		char ch;
		if (fin.is_open())
		{	
			while (fin.get(ch)) // nacitava subor po znaku uklada do 2 rozmerneho vectoru....
			{	
				int i = (int)ch;
				if (ch == '\n')
				{
					y++;
					x = 0;
					continue;
				}
				m_readMaze[x][y] = i-48;
				x++;
			}

			fin.close();
		}
	}
	
	//------------------------------------------------------------------------
	// destruktor
	~CFileReader()
	{
		
	}
	
	//------------------------------------------------------------------------
	// getter, vracia bludisko...
	vector<vector<int> > & getMaze() { return m_readMaze; }
};

