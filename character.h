#pragma once
#include "SDL/SDL.h"

class Ccharacter
{
private:
	int m_x;
	int m_y;
	SDL_Rect m_box;
public: 
	Ccharacter(int x = 0, int y = 0)
	{
		m_x = x;
		m_y = y;
		m_box.x=390;
		m_box.y=300;
		m_box.w=RECTSIZE;
		m_box.h=RECTSIZE;
	}
	
	void draw(SDL_Surface *screen)
	{
		SDL_FillRect(screen,&m_box, 0x0027DB); // modra
	}
	
	int getX() { return m_x; }
	int getY() { return m_y; }
	void setX(int X)
	{ 
		m_x += X;
	}
	void setY(int Y)
	{ 
		m_y += Y;
	}
};
