projekt bludisko, 

autori Jozef Krcho, Martin Cuka
email, cukamartin@gmail.com

potrebne kniznice na spustenie : SDL-1.2.15 (je pribalena)

prikaz na kompilaciu :  g++ -o Ant AntBrain.cxx -lSDL
na spustenie : ./Ant

Program vytvori nahodne bludisko. Sipkami sa hrac musi dostat do ciela.
Okrem algoritmu na generaciu bludiska mame aj algotritmus na najdenie najkratsej cesty.
Hrac si moze zobrazit cestu (pomoc) von z bludiska stlacenim klavesy "h".
Po prejdeni bludiska sa vygeneruje nahodne nove bludisko.
