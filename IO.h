#pragma once

const int SIZE(23); // velkost bludiska, lubovolne neparne cislo
const int RECTSIZE(30);
enum Values { EMPTY = 0, WALL = 1, BASE = 2, PATH = 3, START = 4, FINISH = 5 };
enum Directions { UP = 0, DOWN = 1, RIGHT = 2, LEFT = 3 };
